import moment from 'moment'
import { cognito, update_table, fetch_table } from "@/helpers/cognito.js"
import router from '@/router/index'
import * as AmazonCognitoIdentity from "amazon-cognito-identity-js"

export const todo = {
    namespaced: true,
    state: {
        todos: [],
        lists: [],
        editTodo: {},
        sorted: 'desc',
        selectedDate: new Date(),
        loading: false
    },
    mutations: {
        SET_LOADING: (state, loading) => {
            state.loading = loading
        },
        ADD_TODO: (state, todo) => {
            state.todos = [...state.todos, todo].sort((a, b) => new Date(b.date) - new Date(a.date)).reverse()
        },
        ADD_LIST: (state, list) => {
            state.lists = [...state.lists, list]
        },
        SET_LISTS: (state, lists) => {
            state.lists = lists
        },
        SET_TODOS: (state, todos) => {
            state.todos = todos
        },
        DELETE_TODO: (state, id) => {
            state.todos = state.todos.filter(todo => todo.id !== id)
        },
        DELETE_LIST: (state, id) => {
            state.lists = state.lists.filter(list => list.id !== id)
            state.todos.forEach(todo => {
                if (todo.list.id === id) todo.list = ''
            })
        },
        DELETE_ALL_TODOS: (state) => {
            state.todos = []
            state.lists = []
            state.editTodo = {}
            state.sorted = 'desc'
            state.selectedDate = new Date()
            state.settings = {fontSize: 'medium'}
        },
        COMPLETE_TODO: (state, id) => {
            const todo = _.find(state.todos, { 'id': id })
            todo.completed = !todo.completed
        },
        SET_EDIT_TODO: (state, todo) => {
            state.editTodo = todo
        },
        UPDATE_TODO: (state, todo) => {
            const edit = _.find(state.todos, { 'id': todo.id })
            edit.completed = todo.completed
            edit.date = todo.date
            edit.notes = todo.notes
            edit.title = todo.title
            edit.priority = todo.priority
            if (edit.list) edit.list = todo.list
        },
        SELECT_DATE: (state, date) => {
            state.selectedDate = date
        },
        SORT_TODOS: (state, method) => {
            if (method === 'desc') state.todos = state.todos.sort((a, b) => new Date(b.date) - new Date(a.date)).reverse()
            if (method === 'asc') state.todos = state.todos.sort((a, b) => new Date(b.date) - new Date(a.date))
            if (method === 'asc-priority') state.todos = state.todos.sort((a, b) => b.priority - a.priority)
            if (method === 'desc-priority') state.todos = state.todos.sort((a, b) => b.priority - a.priority).reverse()
            state.sorted = method
        }
    },
    actions: {
        add_todo( { commit, state, rootState }, { todo, token } ) {
            commit( "ADD_TODO", todo )
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, token )
        },
        delete_todo( { commit, state, rootState }, { id, token } ) {
            commit("DELETE_TODO", id )
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, token )
        },
        update_todo( { commit, state, rootState }, { todo, token } ) {
            commit("UPDATE_TODO", todo )
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, token )
        },
        complete_todo({ commit, state, rootState }, id) {
            commit("COMPLETE_TODO", id)
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, rootState.aws.user.idToken.jwtToken )
        },
        reset_app({ commit, rootState }) {
            commit("DELETE_ALL_TODOS")
            update_table( rootState.aws.user.idToken.payload.email, [], [], rootState.aws.user.idToken.jwtToken )
        },
        delete_list({ commit, state, rootState }, list_id) {
            commit("DELETE_LIST", list_id)
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, rootState.aws.user.idToken.jwtToken )
        },
        add_list({ commit, state, rootState }, list) {
            commit("ADD_LIST", list)
            update_table( rootState.aws.user.idToken.payload.email, state.todos, state.lists, rootState.aws.user.idToken.jwtToken )
        },
        get_todos({ commit, rootState }) {
            commit("SET_LOADING", true)
            fetch_table(rootState.aws.user.idToken.payload.email, rootState.aws.user.idToken.jwtToken)
            .then(res => res.json())
            .then(({ body }) => {
                console.log(body)
                if ( Object.keys(body).length == 0 ) {
                    commit("SET_LISTS", [])
                    commit("SET_TODOS", [])
                }
                else {
                    commit("SET_LISTS", JSON.parse(body.Item.lists))
                    commit("SET_TODOS", JSON.parse(body.Item.todos))    
                }
                commit("SET_LOADING", false)
            })
            .catch(err => {
                console.log("Error fetching state, refreshing...")
                console.log(err)
                let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                    UserPoolId: cognito.user_pool_id,
                    ClientId: cognito.app_client_id,
                })
                const cognito_user = user_pool.getCurrentUser()
                cognito_user.getSession( ( err, data ) => {
                    if ( err ) {
                        commit("SET_LOADING", false)
                        commit("aws/SET_LOGGED_IN", false, { root: true })
                        router.push("/login");
                    } 
                    else {
                        commit("aws/SET_LOGGED_IN", true, { root: true })
                        commit("aws/SET_USER", data, { root: true })
                    }
                })
            })
        },
    },
    getters: {
        total({ todos }) {
            return todos.length
        },
        completed({ todos }) {
            return todos.filter(todo => todo.completed === true).length
        },
        incomplete({ todos }) {
            return todos.filter(todo => todo.completed === false).length
        },
        todayTotal(state, getters) {
            return getters.today.length
        },
        dates({ todos }) {
            return todos.map(todo => new Date(todo.date))
        },
        today({ todos }) {
            return todos.filter(todo => {
                const date = new Date(todo.date)
                const today = new Date()
                return date.getDate() == today.getDate() &&
                    date.getMonth() == today.getMonth() &&
                    date.getFullYear() == today.getFullYear()
            })
        },
        tomorrow({ todos }) {
            return todos.filter(todo => {
                const date = new Date(todo.date)
                const today = new Date()
                return date.getDate() == today.getDate() + 1 &&
                    date.getMonth() == today.getMonth() &&
                    date.getFullYear() == today.getFullYear()
            })
        },
        specificDateTodos({ todos, selectedDate }) {
            const dateSelected = moment(selectedDate).format('DD/MM/YYYY')
            return todos.filter(todo => {
                const todoDate = moment(todo.date).format('DD/MM/YYYY')
                return todoDate === dateSelected
            })
        },
        listTaskCount({ lists, todos }) {
            let obj = {}
            lists.forEach(list => {
                obj[list.name] = {
                    "tasks": todos.filter(todo => todo.list.name === list.name).length
                }
            })
            return obj
        }
    },
}