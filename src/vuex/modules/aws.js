import { cognito, delete_user }   from "@/helpers/cognito.js"
import * as AmazonCognitoIdentity from "amazon-cognito-identity-js"

export const aws = {
    namespaced: true,
    state: {
        logged_in: false,
        user: ""
    },
    mutations: {
        SET_USER: ( state, user ) => {
            state.user = user
        },
        SET_LOGGED_IN: ( state, bool ) => {
            state.logged_in = bool
        },
    },
    actions: {
        AWS_login ( { commit }, { user, password } ) {
            console.log("Logging in...")
            let authentication_details = new AmazonCognitoIdentity.AuthenticationDetails({
                Username: user,
                Password: password,
            })
            let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                UserPoolId: cognito.user_pool_id,
                ClientId: cognito.app_client_id,
            })
            let cognito_user = new AmazonCognitoIdentity.CognitoUser({
                Username: user,
                Pool: user_pool,
            })
            return new Promise( ( resolve, reject ) => {
                cognito_user.authenticateUser(authentication_details, {
                    onSuccess: result => {
                        commit("SET_LOGGED_IN", true)
                        commit("SET_USER", result)
                        resolve()
                    },
                    onFailure: err => {
                        console.log(err)
                        commit("SET_LOGGED_IN", false)
                        reject(err)
                    }
                })
            })
        },
        AWS_signup ( { commit }, { user, password } ) {
            console.log("Registering user...")
            let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                UserPoolId: cognito.user_pool_id,
                ClientId: cognito.app_client_id,
            })
            let att_list = []
            let att_email = new AmazonCognitoIdentity.CognitoUserAttribute({
                Name: "email",
                Value: user,
            })
            att_list.push(att_email)
            return new Promise( ( resolve, reject ) => {
                user_pool.signUp(user, password, att_list, null, ( err, result ) => {
                    if ( err ) {
                        commit("SET_LOGGED_IN", false)
                        reject(err)
                    }
                    else {
                        let cognito_user = result.user
                        console.log("user name is " + cognito_user.getUsername())
                        resolve("Please check your email to confirm your account.")
                    }
                })
            })
        },
        AWS_logout ( { commit } ) {
            console.log("Logging out...")
            let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                UserPoolId: cognito.user_pool_id,
                ClientId: cognito.app_client_id,
            })
            let cognito_user = user_pool.getCurrentUser()
            if (cognito_user != null) {
                return new Promise( ( resolve, reject ) => {
                    cognito_user.getSession( ( err, session ) => {
                        if ( err ) {
                            console.log(err)
                            return
                        }
                        cognito_user.globalSignOut({
                            onSuccess: result => {
                                commit("SET_LOGGED_IN", false)
                                resolve()
                            },
                            onFailure: err => {
                                console.log(err)
                                commit("SET_LOGGED_IN", false)
                                reject(err)
                            }
                        })
                    })
                })
            }
        },
        AWS_delete_account( { state } ) {
            console.log("Deleting account...")
            return delete_user(state.user.idToken.payload.email, state.user.idToken.jwtToken)
            .then(res => res.json())
            .then(json => {
                console.log(json)
            })
            .catch(err => {
                console.log(err)
            })
        },
        AWS_request_reset_password( { state }, {username} ) {
            console.log(`Requesting password change... for ${username}`)
            let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                UserPoolId: cognito.user_pool_id,
                ClientId: cognito.app_client_id,
            })
            let cognito_user = new AmazonCognitoIdentity.CognitoUser({
                Username: username,
                Pool: user_pool
            })
            return new Promise( ( resolve, reject ) => {
                cognito_user.forgotPassword({
                    onSuccess: ( data ) => {
                        console.log(data);
                        resolve()
                    },
                    onFailure: (err) => {
                        console.log("Error requesting password reset")
                        console.log(err)
                        reject("Error requesting password reset. Make sure the email is correct.")
                    }
                })
            })
        },
        AWS_submit_password_reset_code({ state }, { new_password, code, username }) {
            console.log("Submitting password reset code...")
            let user_pool = new AmazonCognitoIdentity.CognitoUserPool({
                UserPoolId: cognito.user_pool_id,
                ClientId: cognito.app_client_id,
            })
            let cognito_user = new AmazonCognitoIdentity.CognitoUser({
                Username: username,
                Pool: user_pool
            })
            return new Promise( ( resolve, reject ) => {
                cognito_user.confirmPassword(code, new_password, {
                    onSuccess: ( data ) => {
                        console.log(data)
                        resolve()
                    },
                    onFailure: ( err ) => {
                        console.log("Error submitting code for reset")
                        console.log(err)
                        reject("Wrong code or password requirements not met.")
                    }
                })
            })
        }
    }
}