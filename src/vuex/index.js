import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import _ from 'lodash'

// Vuex store module imports
import { aws } from "./modules/aws.js"
import { todo } from "./modules/todo.js"

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const store = new Vuex.Store({
  namespaced: true,
  modules: {
    aws,
    todo
  },
  plugins: [vuexLocal.plugin],
  state: {
    settings: {
      fontSize: 'medium'
    }
  },
  mutations: {
    CHANGE_FONT: (state, font) => {
      state.settings.fontSize = font
    }
  },
})

export default store
