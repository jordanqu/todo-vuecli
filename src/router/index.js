import Vue       from 'vue'
import VueRouter from 'vue-router'
import store     from '@/vuex'

import Home      from '../views/Home.vue'
import Add       from '../views/Add.vue'
import Edit      from '../views/Edit.vue'
import Settings  from '../views/Settings.vue'
import Calendar  from '../views/Calendar.vue'
import Lists     from '../views/Lists.vue'
import Login     from '../views/Login.vue'
import CreateAccount from '../views/CreateAccount.vue'
import ResetPassword from '../views/ResetPassword.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Tasks',
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/add',
    name: 'Add',
    component: Add,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/edit',
    name: 'Edit',
    component: Edit,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: Calendar,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/lists',
    name: 'Lists',
    component: Lists,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/create-account',
    name: 'Create Account',
    component: CreateAccount
  },
  {
    path: '/reset-password',
    name: 'Reset Password',
    component: ResetPassword
  }
]

const router = new VueRouter({
  routes
})

//Check for user logged in and route auth before entering route
router.beforeEach((to, from, next) => {
  if ( to.matched.some( record => record.meta.requiresAuth ) ) {
    if ( !store.state.aws.logged_in ) {
      next({ name: 'Login' })
    } 
    else {
      next()
    }
  } 
  else {
    next()
  }
})

export default router
