import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex'

import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

Vue.use(VuejsDialog, {
  okText: 'Continue',
  cancelText: 'Cancel',
  animation: 'fade'
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
