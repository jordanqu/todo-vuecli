export const cognito = {
    app_client_id: "4oqbbusv2h4o136d4adjikt887",
    user_pool_id: "ap-southeast-2_fzf9BB8Dc",
    region: "ap-southeast-2",
    api_url: "https://z5q86aq4rb.execute-api.ap-southeast-2.amazonaws.com/prod"
}


export const update_table = ( user, todos, lists, token ) => {
    console.log("Updating table...")
    fetch(`${cognito.api_url}/add-todo`, {
        method: "post",
        headers: {
            "x-api-key": token
        },
        body: JSON.stringify({
            user,
            todos,
            lists
        })
    })
    .then(res => res.json())
    .then(json => {
        console.log(json)
    })
    .catch(err => {
        alert("Error, need to login again!")
    })
}


export const fetch_table = ( user, token ) => {
    console.log("Fetching state/table...")
    return fetch(`${cognito.api_url}/get-todos`, {
        method: "post",
        headers: {
            "x-api-key": token
        },
        body: JSON.stringify({
            user
        })
    })
}


export const delete_user = ( user, token ) => {
    return fetch(`${cognito.api_url}/delete-user`, {
        method: "post",
        headers: {
            "x-api-key": token,
            "content-type": "application/json"
        },
        body: JSON.stringify({
            user
        })
    })
}