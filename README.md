# Todo app

Backed by AWS Cognito, AWS Lambda, AWS API Gateway and DynamoDB. This combination lets me authenticate users and create authenticated API requests. The app will sync lists and todos to DynamoDB for users to manage their lists ascross platforms.

Users can create an account and add their list of todos. The user will need to verify their email before they are able to login.

  
## Live App


See it in action [here](https://jordanqu.bitbucket.io/).

  
## Project setup


Architecture map/diagram can be found [here](https://lucid.app/documents/view/80e85f2d-f41d-40cb-9cdf-68c27fa6e60e).

![Diagram](https://cdn.shopify.com/s/files/1/0244/9647/2142/files/Screen_Shot_2020-12-01_at_3.54.24_pm.png?v=1606802762)
  

## Project setup

```

$ npm install

```

### Compiles and hot-reloads for development

```

$ npm run serve

```

### Compiles and minifies for production

```

$ npm run build

```

### Lints and fixes files

```

$ npm run lint

```
